<html>
<head>
<meta charset="utf-8"></meta>
<title>Pricelist</title>
</head>
<body>
<?php 
require_once('vendor\autoload.php');
use PhpOffice\PhpSpreadsheet\IOFactory;

$table = IOFactory::load("pricelist.xls");
$sheet = $table -> getSheet(0);
$cells = $sheet -> getCellCollection();
$highestRow = $cells -> getHighestRow();
$highestCol = $cells -> getHighestColumn();

$dbc = mysqli_connect('localhost', 'root', '', 'pricelist')
or die('Error connecting to MySQL server.');
mysqli_set_charset($dbc, "utf8");
$query = "INSERT INTO PRICELIST (NAME, PRICE, PRICEWHSL, DEPOT1, DEPOT2, COUNTRY) VALUES ";

for($row = 2; $row <= $highestRow; $row++)
{
	$query .= "(";
	echo '<tr>';
	for($col = 'A'; $col <= $highestCol; $col++)
	{
		$currentCell = $cells -> get($col . $row);
		$currentCellValue = $currentCell -> getValue();
		if(in_array($col,['B','C','D','E']))
		{
			if(is_numeric($currentCellValue))
				$query.="'$currentCellValue',";
			else
				$query.="'10000',";
		}
		else
			$query.="'$currentCellValue',";
	}
	$query = trim($query, ',');
	$query .= "),";
}
$query = trim($query, ',');
// echo $query;
$result = mysqli_query($dbc, $query);
// echo mysqli_error ($dbc);


$querysel = 'SELECT * FROM pricelist';
$queryMax = 'SELECT MAX(PRICE) FROM pricelist';
$queryMin = 'SELECT MIN(PRICEWHSL) FROM pricelist';
$resultsel = mysqli_query($dbc, $querysel);
$resultnum = mysqli_num_rows($resultsel);
$maxPrice = mysqli_fetch_row(mysqli_query($dbc, $queryMax))[0];
$minPrice = mysqli_fetch_row(mysqli_query($dbc, $queryMin))[0];
$goodsDepot1 = 0;
$goodsDepot2 = 0;
$sumPrice = 0.0;
$sumWhslPrice = 0.0;
echo '<table>';
while($row = mysqli_fetch_assoc($resultsel))
{
	echo '<tr>';
	$goodsDepot1 += $row['DEPOT1'];
	$goodsDepot2 += $row['DEPOT2'];
	$sumPrice += $row['PRICE'];
	$sumWhslPrice += $row['PRICEWHSL'];
	foreach($row as $key => $val)
	{
		if($row['PRICE'] == $maxPrice)
			echo '<td style = "color:red">'.$val.'</td>';
		else if($row['PRICEWHSL'] == $minPrice)
			echo '<td style = "color:green">'.$val.'</td>';
		else
			echo '<td>'.$val.'</td>';
	}
	if($row['DEPOT1'] + $row['DEPOT2'] < 20)
		echo '<td>Осталось мало!! Срочно докупите!!!</td>';
	else
		echo '<td></td>';
	echo '</tr>';
}
$avePrice = number_format($sumPrice/$resultnum, 2, '.','');
$aveWhslPrice = number_format($sumWhslPrice/$resultnum, 2, '.','');
echo '<tr>';
echo "<td></td><td>$avePrice</td><td>$aveWhslPrice</td><td>$goodsDepot1</td><td>$goodsDepot2</td><td></td>";
echo '</tr>';
echo '</table>';


mysqli_close($dbc);
?>
</body>
</html>